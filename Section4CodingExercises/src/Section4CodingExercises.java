public class Section4CodingExercises {

	public static void main(String[] args) {
		String spacer = new String(new char[72]).replace('\0', '*');

		MegaBytesConverter.printMegaBytesAndKiloBytes(-1); // Invalid Value
		MegaBytesConverter.printMegaBytesAndKiloBytes(0); // 0 KB = 0 MB and 0 KB
		MegaBytesConverter.printMegaBytesAndKiloBytes(1); // 1 KB = 0 MB and 1 KB
		MegaBytesConverter.printMegaBytesAndKiloBytes(1024); // 1024 KB = 1 MB and 0 KB
		MegaBytesConverter.printMegaBytesAndKiloBytes(2050); // 2050 KB = 2 MB and 2 KB

		System.out.println(spacer);

		System.out.println(BarkingDog.bark(false, -1)); // false
		System.out.println(BarkingDog.bark(true, -1)); // false
		System.out.println(BarkingDog.bark(false, 8)); // false
		System.out.println(BarkingDog.bark(false, 22)); // false
		System.out.println(BarkingDog.bark(true, 8)); // false
		System.out.println(BarkingDog.bark(true, 22)); // false
		System.out.println(BarkingDog.bark(true, 7)); // true
		System.out.println(BarkingDog.bark(true, 23)); // true
		System.out.println(BarkingDog.bark(true, 9)); // false
		System.out.println(BarkingDog.bark(true, 21)); // false

		System.out.println(spacer);

		System.out.println(LeapYear.isLeapYear(-1600)); // false
		System.out.println(LeapYear.isLeapYear(1600)); // true
		System.out.println(LeapYear.isLeapYear(2018)); // false
		System.out.println(LeapYear.isLeapYear(2000)); // true

		System.out.println(spacer);

		System.out.println(DecimalComparator.areEqualByThreeDecimalPlaces(-3.1756, -3.175)); // true
		System.out.println(DecimalComparator.areEqualByThreeDecimalPlaces(3.175, 3.176)); // false
		System.out.println(DecimalComparator.areEqualByThreeDecimalPlaces(3.0, 3.0)); // true

		System.out.println(spacer);

		System.out.println(EqualSumChecker.hasEqualSum(1, 1, 1)); // false
		System.out.println(EqualSumChecker.hasEqualSum(1, 1, 2)); // true
		System.out.println(EqualSumChecker.hasEqualSum(1, -1, 0)); // true

		System.out.println(spacer);

		System.out.println(TeenNumberChecker.hasTeen(9, 99, 19)); // true
		System.out.println(TeenNumberChecker.hasTeen(23, 15, 42)); // true
		System.out.println(TeenNumberChecker.hasTeen(22, 23, 34)); // false

		System.out.println(spacer);

		System.out.println(SecondsAndMinutes.getDurationString(-1, 0)); // Invalid value
		System.out.println(SecondsAndMinutes.getDurationString(0, -1)); // Invalid value
		System.out.println(SecondsAndMinutes.getDurationString(0, 0)); // 00h 00m 00s
		System.out.println(SecondsAndMinutes.getDurationString(61, 0)); // 01h 01m 00s
		System.out.println(SecondsAndMinutes.getDurationString(549, 9)); // 09h 09m 09s
		System.out.println(SecondsAndMinutes.getDurationString(-1)); // Invalid value
		System.out.println(SecondsAndMinutes.getDurationString(0)); // 00h 00m 00s
		System.out.println(SecondsAndMinutes.getDurationString(61)); // 00h 01m 01s
		System.out.println(SecondsAndMinutes.getDurationString(3661)); // 01h 01m 01s
		System.out.println(SecondsAndMinutes.getDurationString(36610)); // 10h 10m 10s

		System.out.println(spacer);

		System.out.println(AreaCalculator.area(5.0)); // 78.53975
		System.out.println(AreaCalculator.area(-1)); // -1
		System.out.println(AreaCalculator.area(5.0, 4.0)); // 20.0
		System.out.println(AreaCalculator.area(-1.0, 4.0)); // -1

		System.out.println(spacer);

		MinutesToYearsDaysCalculator.printYearsAndDays(-1); // Invalid Value
		MinutesToYearsDaysCalculator.printYearsAndDays(525600); // 525600 min = 1 y and 0 d
		MinutesToYearsDaysCalculator.printYearsAndDays(1051200); // 1051200 min = 2 y and 0 d
		MinutesToYearsDaysCalculator.printYearsAndDays(561600); // 561600 min = 1 y and 25 d

		System.out.println(spacer);

		IntEqualityPrinter.printEqual(1, 1, 1); // All numbers are equal
		IntEqualityPrinter.printEqual(1, 1, 2); // Neither all are equal or different
		IntEqualityPrinter.printEqual(-1, -1, -1); // Invalid Value
		IntEqualityPrinter.printEqual(1, 2, 3); // All numbers are different

		System.out.println(spacer);

		System.out.println(PlayingCat.isCatPlaying(true, 10)); // false
		System.out.println(PlayingCat.isCatPlaying(false, 36)); // false
		System.out.println(PlayingCat.isCatPlaying(false, 35)); // true
	}

}

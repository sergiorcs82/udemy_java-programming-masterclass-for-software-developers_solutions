public class AreaCalculator {
	
	protected static final double PI = 3.14159;

	public static double area(double radius) {
		if (radius < 0) {
			return -1;
		}

		return radius * radius * PI;
	}

	public static double area(double width, double height) {
		if (width < 0 || height < 0) {
			return -1;
		}

		return width * height;
	}

}

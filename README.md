# Udemy - Java Programming Masterclass for Software Developers - Solutions
My solutions for [Tim Buchalka's "Java Programming Masterclass for Software Developers" course on Udemy](https://www.udemy.com/java-the-complete-java-developer-course/ "Java Programming Masterclass for Software Developers | Udemy").

## Disclaimer

- I (@sergiorcs82) do not own any part of the aforementioned course.
- These are just the solutions i tried to find on my own. They may not work at all; and even if they do, there may be better ways to solve those problems.
